package com.edugaon.autophoneauth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity() {
    private var firebaseAuth = FirebaseAuth.getInstance()
    private lateinit var phoneAuthProvider : PhoneAuthProvider
    private lateinit var phoneEditText: EditText
    private lateinit var otp: EditText
    private lateinit var sendOtpButton: Button
    private lateinit var verifyOTP: Button
    private lateinit var progressBar: ProgressBar
    private var verificationId = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         phoneEditText = findViewById(R.id.enter_phone_number)
         sendOtpButton = findViewById(R.id.SendOTP)
         otp = findViewById(R.id.enter_otp)
         verifyOTP = findViewById(R.id.verifyOTP)
         progressBar = findViewById(R.id.progressBar)
        phoneAuthProvider = PhoneAuthProvider.getInstance()

        sendOtpButton.setOnClickListener {
            val phone = phoneEditText.text.toString().trim()

            progressBar.visibility = View.VISIBLE
            phoneAuthProvider.verifyPhoneNumber("+91$phone",60,TimeUnit.SECONDS, this, mCallBack)
        }

        verifyOTP.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            val credential = PhoneAuthProvider.getCredential(verificationId,otp.text.toString().trim())
            signInWithCredential(credential)
        }
    }
    // if you are doing auto phone verification then your firebase project should be connected from google cloud console
    val mCallBack = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {

            progressBar.visibility = View.VISIBLE
            phoneEditText.visibility = View.GONE
            sendOtpButton.visibility = View.GONE
            otp.visibility = View.VISIBLE
            verifyOTP.visibility = View.VISIBLE
            otp.setText(phoneAuthCredential.smsCode)
            signInWithCredential(phoneAuthCredential)
        }

        override fun onVerificationFailed(exeption: FirebaseException) {
            progressBar.visibility = View.GONE
            Toast.makeText(this@MainActivity, exeption.message, Toast.LENGTH_SHORT).show()
        }

        override fun onCodeSent(
            storedVerificationCode: String,
            token: PhoneAuthProvider.ForceResendingToken
        ) {
            super.onCodeSent(storedVerificationCode, token)
            verificationId = storedVerificationCode
            progressBar.visibility = View.GONE
            phoneEditText.visibility = View.GONE
            sendOtpButton.visibility = View.GONE
            otp.visibility = View.VISIBLE
            verifyOTP.visibility = View.VISIBLE
        }
        }

        private fun signInWithCredential(credential: PhoneAuthCredential) {
            firebaseAuth.signInWithCredential(credential)
                .addOnFailureListener {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@MainActivity, "success", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this@MainActivity, "Failed", Toast.LENGTH_SHORT).show()

                }
        }
    }
